use crate::cli::base;

mod cli;
mod vault;

#[derive(Debug)]
struct PsmError(String);

fn main() -> Result<(), PsmError> {
    match base::run_cli() {
        Ok(_) => Ok(()),
        Err(e) => Err(PsmError(e)),
    }
}
