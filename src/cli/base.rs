use crate::cli::subcommand::init;
use clap::{Parser, Subcommand};

#[derive(Parser, Debug)]
#[clap(author, version, about)]
struct Args {
    #[clap(subcommand)]
    pub action: Action,
}

#[derive(Subcommand, Debug)]
enum Action {
    Init,
}

pub fn run_cli() -> Result<(), String> {
    let args = Args::parse();
    let subcommand_result = delegate_subcommand(args);

    match subcommand_result {
        Ok(x) => Ok(x),
        Err(e) => Err(e),
    }
}

fn delegate_subcommand(args: Args) -> Result<(), String> {
    match &args.action {
        Action::Init => init::init_password_vault(),
    }
}
