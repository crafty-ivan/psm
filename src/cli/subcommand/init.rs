use rpassword;

use crate::vault::vault;

pub fn init_password_vault() -> Result<(), String> {
    if has_existing_vault() {
        return Err(String::from("Password vault already exists in your home directory"));
    }

    let password = prompt_for_password();
    match vault::create(&password) {
        Ok(_) => {
            println!("Vault successfully created at your home directory.");
            Ok(())
        },
        Err(e) => Err(e),
    }
}

fn has_existing_vault() -> bool {
    let vault_dir = vault::resolve_default_vault_path().unwrap();
    vault_dir.as_path().exists()
}

#[allow(unused_assignments)]
fn prompt_for_password() -> String {
    let mut password_candidate = String::new();
    // Prompt for password the first time
    loop {
        password_candidate = rpassword::prompt_password("Enter the password for your vault: ").unwrap();

        match is_password_strong(&password_candidate) {
            true => break,
            false => println!("Your password must be at least 12 characters long and have at least one letter! Try another one..."),
        }
    }

    // Prompt the user to retype the password
    let mut retyped_password = String::new();
    loop {
        retyped_password = rpassword::prompt_password("Reenter your password: ").unwrap();

        match retyped_password.eq(&password_candidate) {
            true => break,
            false => println!("Didn't enter the same password! Try again...")
        }
    }

    password_candidate
}

fn is_password_strong(password_candidate: &String) -> bool {
    // Criteria: password must be at least 12 characters long
    // and must have at least one digit
    password_candidate.len() >= 12
        && password_candidate.contains(|x: char| x.is_numeric())
}
