use std::{collections::HashMap, io::Write};
use std::fs::File;
use std::io::LineWriter;
use std::path::PathBuf;
use home;
use scrypt::{
    password_hash::{
        rand_core,
        PasswordHasher,
        PasswordHash,
        SaltString,
    },
    Scrypt
};

pub fn create(vault_password: &String) -> Result<Manager, String> {    
    let salt = SaltString::generate(&mut rand_core::OsRng);
    let password_hash = Scrypt.hash_password(vault_password.as_bytes(), &salt).unwrap();

    let vault_result = create_vault_file(&password_hash, &salt);

    match vault_result {
        Ok(_) => Ok(Manager {
            passwords: HashMap::new(),
            vault_salt: salt.to_string(),
            vault_hashed_passwd: password_hash.serialize().to_string(),
        }),
        Err(e) => Err(String::from(e)),
    }
}

pub fn resolve_default_vault_path() -> Option<PathBuf> {
    let home_dir = home::home_dir();

    match home_dir {
        Some(mut x) => {
            x.push(PathBuf::from(".psmvault"));
            Some(x)
        },
        None => None,
    }
}

fn create_vault_file(password_hash: &PasswordHash, salt: &SaltString) -> Result<(), String> {
    let vault_file = File::create(resolve_default_vault_path().unwrap().as_path()).unwrap();
    let mut vault_file = LineWriter::new(vault_file);

    vault_file.write_all(salt.as_bytes()).unwrap();
    vault_file.write_all(b"\n").unwrap();

    vault_file.write_all(password_hash.serialize().as_bytes()).unwrap();
    vault_file.write_all(b"\n").unwrap();

    Ok(())
}

pub struct Manager {
    passwords: HashMap<String, String>,
    vault_salt: String,
    vault_hashed_passwd: String,
}

impl Manager {
    // TODO: password manipulation things will go here
}

