psm is a command-line password manager. It stores your credentials locally behind a single password that you define.

## Installation

(*to be added*)
The plan is to be able to download an installer and to install it via homebrew.

## Getting Started

To begin storing credentials, type `psm init`. You will be prompted to input a master password which you will use to further interact with psm. The stronger the master password, the better, but make sure you remember it! With it, an encrypted file will be created at the path `~/.psm/store` where future entries will be stored.

### Master Password

The master password will need to be periodically re-typed to maintain a psm session. By default, this is every 5 minutes. (to be added: this interval can be configured)

### Storing Passwords

You may save passwords with the command `psm set [name] [password]`. The `[name]` argument should be unique. Two passwords cannot be stored under the same name. To check which names are already present, type `psm ls`.

### Retrieving Passwords

As above, to remember which credentials are stored, type `psm ls` to see names for already saved passwords. Then, retrieve the password you desire with `psm get [name]`. The password will be printed to the console, from which you can copy and paste it.

## Command Reference

- `psm init`: Initializes a password vault under your home directory, creating `~/.psmvault`. Asks you to provide a master password, which you will need to further interact with Does nothing if a store already exists.
- `psm set [name] [password]`: Stores the `[password]` under the `[name]` you want to associate it with, like "Gmail" or "GitLab".
- `psm get [name]`: Prints the password stored under the provided `[name]` to `stdout`.
- `psm rm [name]`: Deletes the password stored under `[name]`.
- (coming soon) `psm edit [name]`: Change the entry under `[name]`, either the name or the password or both.
